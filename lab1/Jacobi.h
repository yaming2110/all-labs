#pragma once
#include "SparseMatrix.h"
#include "GoodThings.h"
#include "Poisson.h"

void jacobiIteration(sparseMatrix* A, double* diag, double* b, double* xn_1, double* xn);
double* JacobiMethod(sparseMatrix* A, sparseMatrix* A_diag, double* diag, double* b, double* x0, double epsilon);
double* JacobiParallelMethod(sparseMatrix* A, sparseMatrix* A_diag, double* diag, double* b, double* x0, double epsilon, ParallelParameters* param);
void PoissonJacobiParallelMethod(PoissonProblem* problem, ParallelParameters* param);