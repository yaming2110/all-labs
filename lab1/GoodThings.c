#include "GoodThings.h"
#include <stdlib.h>

double* cutVector(double* Vec, int startStr, int numStr) {

	double* x = (double*)malloc(numStr * sizeof(double));
	for (int i = 0; i < numStr; i++) {
		x[i] = Vec[i + startStr];
	}
	return x;
}

ParallelParameters* setParallelParameters(int N, int numtasks, int rank) {
	ParallelParameters* param = (ParallelParameters*)malloc(sizeof(ParallelParameters));
	param->rank = rank;
	param->numtasks = numtasks;
	int k = N / numtasks;
	int q = N - numtasks * k;
	if (rank < q) {
		param->startProcStr = (k + 1) * rank;
		param->numProcStr = k + 1;
	}
	else {
		param->startProcStr = (k + 1) * q + k * (rank - q);
		param->numProcStr = k;
	}
	param->sendcounts = (int*)malloc(numtasks * sizeof(int));
	param->recvcounts = (int*)malloc(numtasks * sizeof(int));
	param->rdispls = (int*)malloc(numtasks * sizeof(int));
	param->sdispls = (int*)malloc(numtasks * sizeof(int));
	int sum = 0;
	for (int i = 0; i < numtasks; i++) {
		param->sdispls[i] = 0;
		param->rdispls[i] = sum;
		param->sendcounts[i] = param->numProcStr;
		if (i < q) {
			sum += k + 1;
			param->recvcounts[i] = k + 1;
		}
		else {
			sum += k;
			param->recvcounts[i] = k;
		}
	}
	return param;
}

void freeParallelParameters(ParallelParameters* param) {
	free(param->sendcounts);
	free(param->recvcounts);
	free(param->rdispls);
	free(param->sdispls);
	free(param);
}