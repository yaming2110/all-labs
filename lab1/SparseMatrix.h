#pragma once


typedef struct sparseElement {
    int placeInStr;
    double value;
}sparseElement;

typedef struct {
    int N;
    int M;
    sparseElement* allElements;
    int* StrStart;
    int* strNumberElements;
    int numberOfElements;
}sparseMatrix;

sparseMatrix* createSparseMatrix(int n, int m, int numEl);
sparseMatrix* cutSparseMatrix(sparseMatrix* A, int startStr, int numStr);
sparseMatrix* cutDiag(sparseMatrix* A, double* diag);
void freeSparseMatrix(sparseMatrix* A);
sparseMatrix* create3diagMatrix(int n);
void sparseMatrixMultVector(sparseMatrix* A, double* b, double* x);
void printMatrix(sparseMatrix* A);
void printProperties(sparseMatrix* A);
