#include <stdio.h>
#include <stdlib.h>
#include "string.h"
#include "Poisson.h"
#include "math.h"

PoissonProblem* createPoissonProblem(char** argv) {
	PoissonProblem* problem = (PoissonProblem*)malloc(sizeof(PoissonProblem));
	sparseElement k;
	double xs = atof(argv[1]);
	double ys = atof(argv[2]);
	double xf = atof(argv[3]);
	double yf = atof(argv[4]);
	int N = atoi(argv[5]);
	int M = atoi(argv[6]);
	double eps = atof(argv[7]);
	double hx = (xf - xs) / (N - 1);
	double hy = (yf - ys) / (M - 1);
	problem->A = createSparseMatrix(N * M, N * M, N * M + 4 * (N - 2) * (M - 2));
	problem->diag = (double*)malloc(N * M * sizeof(double));
	problem->b = (double*)malloc(N * M * sizeof(double));
	problem->xn_1 = (double*)malloc(N * M * sizeof(double));
	problem->xn = NULL;
	problem->epsilon = eps;
	int iterator = 0;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			problem->A->StrStart[i * M + j] = iterator;
			problem->xn_1[i * M + j] = 0;
			if (i == 0 || i == N - 1 || j == 0 || j == M - 1) {
				k.placeInStr = i * M + j;
				k.value = 1;
				problem->A->allElements[iterator] = k;
				iterator++;
				problem->A->strNumberElements[i * M + j] = 1;
				problem->b[i * M + j] = sin(i * hx) * exp(j * hy);
			}
			else {
				k.placeInStr = (i - 1) * M + j;
				k.value = 1/(hx * hx);
				problem->A->allElements[iterator] = k;
				iterator++;
				k.placeInStr = i * M + j - 1;
				k.value = 1 / (hy * hy);
				problem->A->allElements[iterator] = k;
				iterator++;
				k.placeInStr = i * M + j;
				k.value = -2 / (hy * hy) - 2 / (hx * hx);
				problem->A->allElements[iterator] = k;
				iterator++;
				k.placeInStr = i * M + j + 1;
				k.value = 1 / (hy * hy);
				problem->A->allElements[iterator] = k;
				iterator++;
				k.placeInStr = (i + 1) * M + j;
				k.value = 1 / (hx * hx);
				problem->A->allElements[iterator] = k;
				iterator++;
				problem->A->strNumberElements[i * M + j] = 5;
				problem->b[i * M + j] = 0;
			}
		}
	}
	problem->A_diag = cutDiag(problem->A, problem->diag);
	return problem;
}

PoissonProblem* cutPoissonProblem(PoissonProblem* problem, ParallelParameters* param) {
	PoissonProblem* cutproblem = (PoissonProblem*)malloc(sizeof(PoissonProblem));
	cutproblem->b = cutVector(problem->b, param->startProcStr, param->numProcStr);
	cutproblem->diag = cutVector(problem->diag, param->startProcStr, param->numProcStr);
	cutproblem->A = cutSparseMatrix(problem->A, param->startProcStr, param->numProcStr);
	cutproblem->A_diag = cutSparseMatrix(problem->A_diag, param->startProcStr, param->numProcStr);
	cutproblem->epsilon = problem->epsilon;
	cutproblem->xn_1 = (double*)malloc(problem->A->N * sizeof(double));
	memcpy(cutproblem->xn_1, problem->xn_1, problem->A->N * sizeof(double));
	return cutproblem;
}

void freePoissonProblem(PoissonProblem* problem) {
	freeSparseMatrix(problem->A);
	freeSparseMatrix(problem->A_diag);
	free(problem->diag);
	free(problem->b);
	free(problem->xn_1);
	if(problem->xn != NULL)
		free(problem->xn);
	free(problem);
}