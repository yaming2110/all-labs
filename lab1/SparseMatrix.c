#include "SparseMatrix.h"
#include <stdio.h>
#include "stdlib.h"


sparseMatrix* createSparseMatrix(int n, int m, int numEl)
{
	sparseMatrix* A = (sparseMatrix*)malloc(sizeof(sparseMatrix));
	A->N = n;
	A->M = m;
	A->numberOfElements = numEl;
	A->allElements = (sparseElement*)malloc(A->numberOfElements * sizeof(sparseElement));
	A->strNumberElements = (int*)malloc(A->N * sizeof(int));
	A->StrStart = (int*)malloc(A->N * sizeof(int));
	return A;

}

sparseMatrix* cutSparseMatrix(sparseMatrix* A, int startStr, int numStr) {
	int numEl = 0;
	for (int i = 0; i < numStr; i++) {
		int buf = A->strNumberElements[startStr + i];
		numEl += buf;
	}
	sparseMatrix* B = createSparseMatrix(numStr, A->M, numEl);
	int BstartStr = 0;
	int iterator = 0;
	for (int i = 0; i < numStr; i++) {
		B->strNumberElements[i] = A->strNumberElements[startStr + i];
		B->StrStart[i] = BstartStr;
		BstartStr += B->strNumberElements[i];
		for (int j = 0; j < B->strNumberElements[i]; j++) {
			B->allElements[iterator] = A->allElements[A->StrStart[startStr + i] + j];
			iterator++;
		}
	}
	return B;
}

sparseMatrix* cutDiag(sparseMatrix* A, double* diag) {
	sparseMatrix* B = createSparseMatrix(A->N, A->M, A->numberOfElements - A->N);
	B->N = A->N;
	B->M = A->M;
	int numElem = 0;
	for (int i = 0; i < A->N; i++) {
		B->StrStart[i] = numElem;
		int strElem = 0;
		for (int j = 0; j < A->strNumberElements[i]; j++) {
			sparseElement buf = A->allElements[A->StrStart[i] + j];
			if (i == buf.placeInStr) {
				diag[i] = buf.value;
			}
			else {
				B->allElements[numElem] = buf;
				numElem++;
				strElem++;
			}
		}
		B->strNumberElements[i] = strElem;
	}
	B->numberOfElements = numElem;
	return B;
}
void freeSparseMatrix(sparseMatrix* A)
{
	free(A->allElements);
	free(A->strNumberElements);
	free(A->StrStart);
	free(A);
}

sparseMatrix* create3diagMatrix(int n)
{
	sparseMatrix* A = createSparseMatrix(n, n, 3 * n - 2);
	sparseElement k;
	int elemCount = 0;
	for (int i = 0; i < A->N; i++) {
		A->StrStart[i] = elemCount;
		if (i != 0) {
			k.placeInStr = i - 1;
			k.value = 1;
			A->allElements[elemCount] = k;
			elemCount++;
		}
		k.placeInStr = i;
		k.value = 2;
		A->allElements[elemCount] = k;
		elemCount++;
		if (i != A->N - 1) {
			k.placeInStr = i + 1;
			k.value = 1;
			A->allElements[elemCount] = k;
			elemCount++;
		}
		A->strNumberElements[i] = elemCount - A->StrStart[i];
	}
	return A;
}

void sparseMatrixMultVector(sparseMatrix* A, double* b, double* x) {
	for (int i = 0; i < A->N; i++) {
		x[i] = 0;
		for (int j = 0; j < A->strNumberElements[i]; j++) {
			sparseElement buf = A->allElements[A->StrStart[i] + j];
			x[i] += buf.value * b[buf.placeInStr];
		}
	}
}

void printMatrix(sparseMatrix* A)
{
	for (int i = 0; i < A->N; i++) {
		int k = A->StrStart[i];
		int num = A->strNumberElements[i];
		for (int j = 0; j < A->M; j++) {
			if (num != 0 && A->allElements[k].placeInStr == j) {
				printf("%f ", A->allElements[k].value);
				k++;
				num--;
			}
			else {
				printf("0 ");
			}
			
		}
		printf("\n");
	}
}

void printProperties(sparseMatrix* A) {
	printf("N = %d, M = %d, number of elements = %d\n", A->N, A->M, A->numberOfElements);
	for (int i = 0; i < A->N; i++) {
		printf("%d str, %d start, %d elements\n", i, A->StrStart[i], A->strNumberElements[i]);
	}
}
