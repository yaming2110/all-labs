#include <stdio.h>
#include "mpi.h"
#include "SparseMatrix.h"
#include "Jacobi.h"
#include <stdlib.h>
#include "string.h"
#include "GoodThings.h"
#include "Poisson.h"
int main(int argc, char** argv)
{

	int numtasks, rank, buf = 0;
	if (argc == 8) {
		MPI_Init(&argc, &argv);
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		MPI_Comm_size(MPI_COMM_WORLD, &numtasks);


		PoissonProblem* problem = createPoissonProblem(argv);
		ParallelParameters* param = setParallelParameters(problem->A->N, numtasks, rank);
		PoissonProblem* parallelProblem = cutPoissonProblem(problem, param);
		freePoissonProblem(problem);
		double timeStart = 0;
		double timeFinish = 0;
		timeStart = MPI_Wtime();
		PoissonJacobiParallelMethod(parallelProblem, param);
		timeFinish = MPI_Wtime();
		double firstTimeStart = 0;
		double lastTimeFinish = 0;
		MPI_Reduce(&timeStart, &firstTimeStart, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
		MPI_Reduce(&timeFinish, &lastTimeFinish, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
		if (rank == 0) {
			FILE* file = NULL;
			fopen_s(&file, "result.txt", "w");
			for (int i = 0; i < parallelProblem->A->M; i++) {
				fprintf(file, "%lf\n", parallelProblem->xn_1[i]);
			}
			fclose(file);
			char fileName[] = "timesStamps_p01.txt";
			fileName[13] = '0' + numtasks / 10;
			fileName[14] = '0' + numtasks % 10;
			fopen_s(&file, fileName, "a");
			fprintf(file, "%lf\n", lastTimeFinish - firstTimeStart);
			fclose(file);
		}


		freePoissonProblem(parallelProblem);
		freeParallelParameters(param);
		MPI_Finalize();

	}
	return 0;
}