#include "Jacobi.h"
#include "math.h"
#include "string.h"
#include "stdlib.h"
#include"mpi.h"
#include <stdio.h>

#define CONTINUE_COMPUTING 228
#define STOP_COMPUTING 1337

void JacobiIteration(sparseMatrix* A, double* diag, double* b, double* xn_1, double* xn) {

	for (int i = 0; i < A->N; i++) {
		xn[i] = b[i];
		sparseElement buf;
		for (int j = 0; j < A->strNumberElements[i]; j++) {
			buf = A->allElements[A->StrStart[i] + j];
			xn[i] -= buf.value * xn_1[buf.placeInStr];
		}
		xn[i] /= diag[i];
	}
}

double* JacobiMethod(sparseMatrix* A, sparseMatrix* A_diag, double* diag, double* b, double* x0, double epsilon) {
	double* xn_1 = x0;
	double* xn = (double*)malloc(A->N * sizeof(double));
	double err = 1;
	double neviaska = 1;
	while (err > epsilon || neviaska > epsilon) {
		JacobiIteration(A_diag, diag, b, xn_1, xn);
		err = 0;
		for (int i = 0; i < A->N; i++) {
			if(err < fabs(xn[i] - xn_1[i]))
				err = fabs(xn[i] - xn_1[i]);
		}
		
		sparseMatrixMultVector(A, xn, xn_1);
		neviaska = 0;
		for (int i = 0; i < A->N; i++) {
			if (neviaska < fabs(xn_1[i] - b[i]))
				neviaska = fabs(xn_1[i] - b[i]);
		}
		memcpy(xn_1, xn, A->N * sizeof(double));
	}
	return xn;
}

double* JacobiParallelMethod(sparseMatrix* A, sparseMatrix* A_diag, double* diag, double* b, double* x0, double epsilon, ParallelParameters* param) {
	double* xn_1 = x0;
	double* xn = (double*)malloc(A->N * sizeof(double));
	double err = 1;
	double localErr;
	double residual = 1;
	double localresidual;
	int flag = 1;
	MPI_Status status;
	while (flag) {
		JacobiIteration(A_diag, diag, b, xn_1, xn);
		localErr = 0;
		for (int i = 0; i < A->N; i++) {
			if (localErr < fabs(xn[i] - xn_1[i + param->startProcStr]))
				localErr = fabs(xn[i] - xn_1[i + param->startProcStr]);
		}
		MPI_Reduce(&localErr, &err, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
		MPI_Alltoallv(xn, param->sendcounts, param->sdispls, MPI_DOUBLE, xn_1, param->recvcounts, param->rdispls, MPI_DOUBLE, MPI_COMM_WORLD);
		sparseMatrixMultVector(A, xn_1, xn);
		localresidual = 0;
		for (int i = 0; i < A->N; i++) {
			if (localresidual < fabs(xn[i] - b[i]))
				localresidual = fabs(xn[i] - b[i]);
		}
		MPI_Reduce(&localresidual, &residual, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
		if (param->rank == 0) {
			int tag = 0;
			if (err > epsilon || residual > epsilon) {
				tag = CONTINUE_COMPUTING;
				flag = 1;
			}
			else {
				tag = STOP_COMPUTING;
				flag = 0;
			}
			for (int i = 1; i < param->numtasks; i++) {
				MPI_Send(NULL, 0, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
			}
		}
		else {
			MPI_Recv(NULL, 0, MPI_DOUBLE, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			if (status.MPI_TAG == CONTINUE_COMPUTING) {
				flag = 1;
			}
			else if(status.MPI_TAG == STOP_COMPUTING){
				flag = 0;
			}
			else {
				printf("Unknown tag");
				flag = 0;
			}
		}
	}
	return xn;
}

void PoissonJacobiParallelMethod(PoissonProblem* problem, ParallelParameters* param) {
	problem->xn = (double*)malloc(problem->A->N * sizeof(double));
	double err = 1;
	double localErr;
	double residual = 1;
	double localresidual;
	int flag = 1;
	MPI_Status status;
	while (flag) {
		JacobiIteration(problem->A_diag, problem->diag, problem->b, problem->xn_1, problem->xn);
		localErr = 0;
		for (int i = 0; i < problem->A->N; i++) {
			if (localErr < fabs(problem->xn[i] - problem->xn_1[i + param->startProcStr]))
				localErr = fabs(problem->xn[i] - problem->xn_1[i + param->startProcStr]);
		}
		MPI_Reduce(&localErr, &err, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
		MPI_Alltoallv(problem->xn, param->sendcounts, param->sdispls, MPI_DOUBLE, problem->xn_1, param->recvcounts, param->rdispls, MPI_DOUBLE, MPI_COMM_WORLD);
		sparseMatrixMultVector(problem->A, problem->xn_1, problem->xn);
		localresidual = 0;
		for (int i = 0; i < problem->A->N; i++) {
			if (localresidual < fabs(problem->xn[i] - problem->b[i]))
				localresidual = fabs(problem->xn[i] - problem->b[i]);
		}
		MPI_Reduce(&localresidual, &residual, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
		if (param->rank == 0) {
			int tag = 0;
			if (err > problem->epsilon || residual > problem->epsilon) {
				tag = CONTINUE_COMPUTING;
				flag = 1;
			}
			else {
				tag = STOP_COMPUTING;
				flag = 0;
			}
			for (int i = 1; i < param->numtasks; i++) {
				MPI_Send(NULL, 0, MPI_DOUBLE, i, tag, MPI_COMM_WORLD);
			}
		}
		else {
			MPI_Recv(NULL, 0, MPI_DOUBLE, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			if (status.MPI_TAG == CONTINUE_COMPUTING) {
				flag = 1;
			}
			else if (status.MPI_TAG == STOP_COMPUTING) {
				flag = 0;
			}
			else {
				printf("Unknown tag");
				flag = 0;
			}
		}
	}
}