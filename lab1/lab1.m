% A=[2,1,0,0;1,2,1,0;0,1,2,1;0,0,1,2];
% % D=diag(diag(A));
% % LU=A-D;
close all;
clear all;
nump = 10;
xs=0;
ys=0;
xf=1;
yf=1;
N = 100;
M = 100;
epsilon = 0.000001;
str = " -np " + num2str(nump) + " lab1.exe "+ num2str(xs) + " "+ num2str(ys) + " "+ num2str(xf) + " "+ num2str(yf) + " "+ num2str(N) + " "+ num2str(M) + " " + num2str(epsilon);
system(strcat("C:\""Program Files""\""Microsoft MPI""\Bin\mpiexec.exe",str))


fileID = fopen('result.txt','r');
formatSpec = '%f';
z = fscanf(fileID, formatSpec);
fclose(fileID);
% b=[1;2;3;4];
% x=A\b;
x1=linspace(0,1,N);
y1=linspace(0,1,M);
[X,Y]=meshgrid(x1,y1);
Z1=zeros(M,N);
for i = 1:N
    for j = 1:M
        Z1(j,i) = z((i-1)*M + j);
    end
end
Z=sin(X).*exp(Y);
figure;
surf(X,Y,Z1);
xlabel('x');
ylabel('y');
zlabel('u');
figure;
surf(X,Y,abs(Z-Z1));
xlabel('x');
ylabel('y');
zlabel('||u-u_\epsilon||_\infty');
maxp = 12;
timep=zeros(3,maxp);
for i=1:maxp
    k=i
    nump = i;
    str = " -np " + num2str(nump) + " lab1.exe "+ num2str(xs) + " "+ num2str(ys) + " "+ num2str(xf) + " "+ num2str(yf) + " "+ num2str(N) + " "+ num2str(M) + " " + num2str(epsilon);
    for j = 1:20
        l=j
        system(strcat("C:\""Program Files""\""Microsoft MPI""\Bin\mpiexec.exe",str))
    end
    str1 = "timesStamps_p" + num2str((nump - rem(nump,10)) / 10) + num2str(rem(nump,10))+".txt";
    fileID = fopen(str1,'r');
    formatSpec = '%f';
    buf = fscanf(fileID, formatSpec);
    fclose(fileID);
    timep(1,i)=min(buf);
    timep(2,i)=mean(buf);
    timep(3,i)=max(buf);
end
speedupp=timep(2,1) ./timep;
efficiency = speedupp;
for i=1:maxp
    efficiency(:,i) = efficiency(:,i) / (i);
end
x=1:1:maxp;
figure;
plot(x, timep(1,:));
hold on;
grid on;
plot(x, timep(2,:));
plot(x, timep(3,:));
xlabel("p");
title("Time");
legend("Minimal time", "Average time", "Maximal time");

figure;
plot(x, speedupp(1,:));
hold on;
grid on;
plot(x, speedupp(2,:));
plot(x, speedupp(3,:));
xlabel("p");
title("Speedup");
legend("Minimal time", "Average time", "Maximal time");

figure;
plot(x, efficiency(1,:));
hold on;
grid on;
plot(x, efficiency(2,:));
plot(x, efficiency(3,:));
xlabel("p");
title("Efficiency");
legend("Minimal time", "Average time", "Maximal time");