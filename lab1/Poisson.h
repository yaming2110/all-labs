#pragma once
#include "SparseMatrix.h"
#include "GoodThings.h"
typedef struct {
    sparseMatrix* A;
    sparseMatrix* A_diag;
    double* diag;
    double* b;
    double* xn_1;
    double* xn;
    double epsilon;
}PoissonProblem;

PoissonProblem* createPoissonProblem(char** argv);
PoissonProblem* cutPoissonProblem(PoissonProblem* problem, ParallelParameters* param);
void freePoissonProblem(PoissonProblem* problem);
