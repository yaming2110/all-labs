#pragma once
double* cutVector(double* Vec, int startStr, int numStr);


typedef struct {
	int* sendcounts;
	int* recvcounts;
	int* rdispls;
	int* sdispls;
	int numProcStr;
	int startProcStr;
	int rank;
	int numtasks;
}ParallelParameters;


ParallelParameters* setParallelParameters(int N, int numtasks, int rank);
void freeParallelParameters(ParallelParameters* param);