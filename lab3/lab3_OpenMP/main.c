#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "math.h"
#include "PGMImage.h"
#include "GWDT.h"
#include "omp.h"
#include <windows.h>
#include <time.h>

int main(int argc, char const* argv[])
{
    const char* infile;
    const char* iofile;
    int num_threads = 1;
    int flag = 0;

    infile = "Cell_1170_761.pgm";
    iofile = "Cell_1170_761Light.pgm";

    if (argc > 1) {
        num_threads = atoi(argv[1]);
    }
    if (argc > 2) {
        flag = atoi(argv[2]);
    }
    if (argc > 3) {
        infile = argv[3];
    }
    if (argc > 4) {
        iofile = argv[3];
    }
    PGMImage* pgm = openPGM(infile);
    PGMImage* U1 = createU0(pgm);
    omp_set_num_threads(num_threads);
    clock_t begin = clock();
    PGMImage* U2 = GWDT(U1, pgm);
    clock_t end = clock();
    double allTime = (double)(end - begin) / CLOCKS_PER_SEC;
    if (flag) {
        PGMImage* U3 = cutU(U2);
        writePGM(U3, iofile);
        freePGM(U3);
    }


    FILE* file = NULL;
    char fileName[] = "timesStamps_p01.txt";
    fileName[13] = '0' + num_threads / 10;
    fileName[14] = '0' + num_threads % 10;
    fopen_s(&file, fileName, "a");
    fprintf(file, "%lf\n", allTime);
    fclose(file);


    freePGM(pgm);
    freePGM(U1);
    freePGM(U2);
    return 0;
}