#include "GWDT.h"
#include "math.h"
#include <stdlib.h>
#include "omp.h"


PGMImage* createU0(PGMImage* pgm) {
    PGMImage* U0 = (PGMImage*)malloc(sizeof(PGMImage));
    U0->height = pgm->height + 2;
    U0->width = pgm->width + 2;
    U0->maxValue = pgm->maxValue;
    U0->data = (double*)malloc(U0->height * U0->width * sizeof(double));
    for (int i = 0; i < U0->height; i++) {
        for (int j = 0; j < U0->width; j++) {
            U0->data[i * U0->width + j] = INFINITY;
        }
    }
    /*U0->data[(U0->height - 2) * U0->width + U0->width - 2] = 0.0;*/ //������ ������ ����
    /*U0->data[1 * U0->width + 1] = 0.0;*/ //����� ������� ����
    /*U0->data[1 * U0->width + U0->width / 2] = 0.0;*/ //�������� ������� �������
    U0->data[(U0->height - 2) * U0->width + U0->width / 2] = 0.0; //�������� ������ �������
    return U0;
}

inline double minOf5(double i1, double i2, double i3, double i4, double i5) {
    double iMin = i1;
    if (iMin > i2) {
        iMin = i2;
    }
    if (iMin > i3) {
        iMin = i3;
    }
    if (iMin > i4) {
        iMin = i4;
    }
    if (iMin > i5) {
        iMin = i5;
    }
    return iMin;
}

int comparePGMImage(PGMImage* Un_1, PGMImage* Un) {
    int flag = 0;
    int i,j;
#pragma omp parallel for private(i, j) collapse(2)
    for (i = 1; i < Un->height - 1; i++) {
        for (j = 1; j < Un->width - 1; j++) {
            if (Un_1->data[i * Un->width + j] != Un->data[i * Un->width + j]) {
                flag++;
            }
        }
    }
    return flag;
}


PGMImage* GWDT(PGMImage* Un, PGMImage* pgm) {
    PGMImage* Un_1 = (PGMImage*)malloc(sizeof(PGMImage));
    //����� ����� �������
    int h = Un->height;
    int i, j, k;
    Un_1->height = h;
    int w = Un->width;
    Un_1->width = w;
    Un_1->maxValue = Un->maxValue;
    Un_1->data = (double*)malloc(Un_1->height * Un_1->width * sizeof(double));
#pragma omp parallel for private(i, j) collapse(2)
    for (i = 0; i < Un_1->height; i++) {
        for (j = 0; j < Un_1->width; j++) {
            Un_1->data[i * Un_1->width + j] = INFINITY;
        }
    }
    PGMImage* buf;
    while (comparePGMImage(Un_1, Un)) {
        buf = Un_1;
        Un_1 = Un;
        Un = buf;

        //�� ������ �������� ����
        int initialILU = 1;
        int initialJLU = 1;
        int pravUsl = 0;
        while (initialILU < Un->height - 1) {
            if (initialILU < pgm->height - (pgm->width + 1) / 2 + 2) {
                pravUsl = (initialJLU + 1) / 2;
            }
            else {
                pravUsl =  (Un->height - 1 - initialILU);
            }
#pragma omp parallel for private(k)
                for (k = 0; k < pravUsl; k++) {
                    int j = initialJLU - k * 2;
                    int i = initialILU + k;
                    double u1 = Un->data[(i - 1) * w + j - 1] + pgm->data[(i - 1) * pgm->width + j - 1] * sqrt(2);
                    double u2 = Un->data[(i - 1) * w + j] + pgm->data[(i - 1) * pgm->width + j - 1] * 1;
                    double u3 = Un->data[(i - 1) * w + j + 1] + pgm->data[(i - 1) * pgm->width + j - 1] * sqrt(2);
                    double u4 = Un->data[i * w + j - 1] + pgm->data[(i - 1) * pgm->width + j - 1] * 1;
                    Un->data[i * w + j] = minOf5(u1, u2, u3, u4, Un_1->data[i * w + j]);
                }
            initialJLU++;
            if (initialJLU == Un->width - 1) {
                initialJLU -= 2;
                initialILU++;
            }
        }

        buf = Un_1;
        Un_1 = Un;
        Un = buf;
        //�� ������� ������� ����
        int initialIRD = Un->height - 2;
        int initialJRD = Un->width - 2;
        while (initialIRD >= 1) {
            if (initialIRD < (pgm->width + 1) / 2) {
                pravUsl = initialIRD;
            }
            else {
                pravUsl = (Un->width - initialJRD) / 2;
            }

#pragma omp parallel for private(k)
            for (k = 0; k < pravUsl; k++) {
                int j = initialJRD + k * 2;
                int i = initialIRD - k;
                double u1 = Un->data[(i + 1) * w + j - 1] + pgm->data[(i - 1) * pgm->width + j - 1] * sqrt(2);
                double u2 = Un->data[(i + 1) * w + j] + pgm->data[(i - 1) * pgm->width + j - 1] * 1;
                double u3 = Un->data[(i + 1) * w + j + 1] + pgm->data[(i - 1) * pgm->width + j - 1] * sqrt(2);
                double u4 = Un->data[i * w + j + 1] + pgm->data[(i - 1) * pgm->width + j - 1] * 1;
                Un->data[i * w + j] = minOf5(u1, u2, u3, u4, Un_1->data[i * w + j]);
            }
            initialJRD--;
            if (initialJRD < 1) {
                initialJRD += 2;
                initialIRD--;
            }
        }
    }
    return Un_1;
}

PGMImage* cutU(PGMImage* U) {
    PGMImage* U0 = (PGMImage*)malloc(sizeof(PGMImage));
    U0->height = U->height - 2;
    U0->width = U->width - 2;
    U0->maxValue = U->maxValue;
    U0->data = (double*)malloc(U0->height * U0->width * sizeof(double));
    for (int i = 0; i < U0->height; i++) {
        for (int j = 0; j < U0->width; j++) {
            U0->data[i * U0->width + j] = U->data[(i+1) * U->width + j+1];
        }
    }
    return U0;
}