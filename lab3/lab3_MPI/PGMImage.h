#pragma once
#include <stdio.h>
typedef struct {
    double* data;
    int width;
    int height;
    int maxValue;
} PGMImage;

PGMImage* createPGMImage(int height, int width);
void ignoreComments(FILE* fp);
PGMImage* openPGM(const char* filename);
void writePGM(PGMImage* pgm, const char* filename);
void freePGM(PGMImage* pgm);
PGMImage* scaleU2(PGMImage* pgm);
PGMImage* scaleD2(PGMImage* pgm);