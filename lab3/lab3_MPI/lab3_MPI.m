close all;
clear all;
maxp = 15;
for i=1:maxp
    k=i
    nump = i;
    str = " -np " + num2str(nump) + " lab3_MPI.exe ";
    for j = 1:20
        l=j
        system(strcat("C:\""Program Files""\""Microsoft MPI""\Bin\mpiexec.exe",str))
    end
    str1 = "timesStamps_p" + num2str((nump - rem(nump,10)) / 10) + num2str(rem(nump,10))+".txt";
    fileID = fopen(str1,'r');
    formatSpec = '%f';
    buf = fscanf(fileID, formatSpec);
    fclose(fileID);
    timep(1,i)=min(buf);
    timep(2,i)=mean(buf);
    timep(3,i)=max(buf);
end
speedupp=timep(2,1) ./timep;
efficiency = speedupp;
for i=1:maxp
    efficiency(:,i) = efficiency(:,i) / (i);
end
x=1:1:maxp;
figure;
plot(x, timep(1,:), 'LineWidth', 2);
hold on;
grid on;
plot(x, timep(2,:), 'LineWidth', 2);
plot(x, timep(3,:), 'LineWidth', 2);
xlabel("p");
title("Time");
legend("Minimal time", "Average time", "Maximal time");

figure;
plot(x, speedupp(1,:), 'LineWidth', 2);
hold on;
grid on;
plot(x, speedupp(2,:), 'LineWidth', 2);
plot(x, speedupp(3,:), 'LineWidth', 2);
xlabel("p");
title("Speedup");
legend("Minimal time", "Average time", "Maximal time");

figure;
plot(x, efficiency(1,:), 'LineWidth', 2);
hold on;
grid on;
plot(x, efficiency(2,:), 'LineWidth', 2);
plot(x, efficiency(3,:), 'LineWidth', 2);
xlabel("p");
title("Efficiency");
legend("Minimal time", "Average time", "Maximal time");