#pragma once
#include "PGMImage.h"


PGMImage* createU0(PGMImage* pgm);
double minOf5(double i1, double i2, double i3, double i4, double i5);
int comparePGMImage(PGMImage* Un_1, PGMImage* Un);
PGMImage* GWDT(PGMImage* Un_1, PGMImage* pgm, int rank, int numtask);
PGMImage* cutU(PGMImage* U);
PGMImage* growU(PGMImage* U);