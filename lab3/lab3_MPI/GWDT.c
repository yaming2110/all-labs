#include "GWDT.h"
#include "math.h"
#include <stdlib.h>
#include "mpi.h"


PGMImage* createU0(PGMImage* pgm) {
    PGMImage* U0 = (PGMImage*)malloc(sizeof(PGMImage));
    U0->height = pgm->height;
    U0->width = pgm->width;
    U0->maxValue = pgm->maxValue;
    U0->data = (double*)malloc(U0->height * U0->width * sizeof(double));
    for (int i = 0; i < U0->height; i++) {
        for (int j = 0; j < U0->width; j++) {
            U0->data[i * U0->width + j] = INFINITY;
        }
    }
    /*U0->data[(U0->height - 1) * U0->width + U0->width - 1] = 0.0;*/ //������ ������ ����
    /*U0->data[0 * U0->width + 0] = 0.0;*/ //����� ������� ����
    /*U0->data[0 * U0->width + U0->width / 2] = 0.0;*/ //�������� ������� �������
    U0->data[(U0->height - 1) * U0->width + U0->width / 2] = 0.0; //�������� ������ �������
    return U0;
}

double minOf5(double i1, double i2, double i3, double i4, double i5) {
    double iMin = i1;
    if (iMin > i2) {
        iMin = i2;
    }
    if (iMin > i3) {
        iMin = i3;
    }
    if (iMin > i4) {
        iMin = i4;
    }
    if (iMin > i5) {
        iMin = i5;
    }
    return iMin;
}

int comparePGMImage(PGMImage* Un_1, PGMImage* Un) {
    int flag = 0;
    for (int i = 1; i < Un->height - 1; i++) {
        for (int j = 1; j < Un->width - 1; j++) {
            if (Un_1->data[i * Un->width + j] != Un->data[i * Un->width + j]) {
                flag++;
            }
        }
    }
    return flag;
}


PGMImage* GWDT(PGMImage* Un, PGMImage* pgm, int rank, int numtask) {
    PGMImage* Un_1 = (PGMImage*)malloc(sizeof(PGMImage));
    //����� ����� �������
    int h = Un->height;
    int i;
    int flag = 1, compareResults = 0;
    Un_1->height = h;
    int w = Un->width;
    Un_1->width = w;
    Un_1->maxValue = Un->maxValue;
    Un_1->data = (double*)malloc(Un_1->height * Un_1->width * sizeof(double));
    for (i = 0; i < Un_1->height; i++) {
        for (int j = 0; j < Un_1->width; j++) {
            Un_1->data[i * Un_1->width + j] = INFINITY;
        }
    }
    PGMImage* buf;
    while (flag) {
        if (rank != numtask - 1) {
            MPI_Send(Un->data + Un->width * (Un->height - 2) + 1, pgm->width, MPI_DOUBLE, rank + 1, rank + 1, MPI_COMM_WORLD);
        }
        buf = Un_1;
        Un_1 = Un;
        Un = buf;
        if (rank != 0) {
            MPI_Status s;
            MPI_Recv(Un->data + 1, pgm->width, MPI_DOUBLE, rank - 1, rank, MPI_COMM_WORLD, &s);
        }
        //�� ������ �������� ����
        int initialILU = 1;
        int initialJLU = 1;
        while (initialILU < Un->height - 1) {
            int j = initialJLU;
            int i = initialILU;
                while(i < Un->height - 1 && j > 0){
                    double u1 = Un->data[(i - 1) * w + j - 1] + pgm->data[(i - 1) * pgm->width + j - 1] * sqrt(2);
                    double u2 = Un->data[(i - 1) * w + j] + pgm->data[(i - 1) * pgm->width + j - 1] * 1;
                    double u3 = Un->data[(i - 1) * w + j + 1] + pgm->data[(i - 1) * pgm->width + j - 1] * sqrt(2);
                    double u4 = Un->data[i * w + j - 1] + pgm->data[(i - 1) * pgm->width + j - 1] * 1;
                    Un->data[i * w + j] = minOf5(u1, u2, u3, u4, Un_1->data[i * w + j]);
                    i++;
                    j-=2;
                }
            initialJLU++;
            if (initialJLU == Un->width - 1) {
                initialJLU -= 2;
                initialILU++;
            }
        }
        if (rank != 0) {
            MPI_Send(Un->data + 1 * Un->width + 1, pgm->width, MPI_DOUBLE, rank - 1, rank - 1, MPI_COMM_WORLD);
        }
        buf = Un_1;
        Un_1 = Un;
        Un = buf;

        if (rank != numtask - 1) {
            MPI_Status s;
            MPI_Recv(Un->data + Un->width * (Un->height - 1) + 1, pgm->width, MPI_DOUBLE, rank + 1, rank, MPI_COMM_WORLD, &s);
        }
        //�� ������� ������� ����
        int initialIRD = Un->height - 2;
        int initialJRD = Un->width - 2;
        while (initialIRD >= 1) {
            int j = initialJRD;
            int i = initialIRD;
            while(i > 0 && j < Un->width - 1) {
                double u1 = Un->data[(i + 1) * w + j - 1] + pgm->data[(i - 1) * pgm->width + j - 1] * sqrt(2);
                double u2 = Un->data[(i + 1) * w + j] + pgm->data[(i - 1) * pgm->width + j - 1] * 1;
                double u3 = Un->data[(i + 1) * w + j + 1] + pgm->data[(i - 1) * pgm->width + j - 1] * sqrt(2);
                double u4 = Un->data[i * w + j + 1] + pgm->data[(i - 1) * pgm->width + j - 1] * 1;
                Un->data[i * w + j] = minOf5(u1, u2, u3, u4, Un_1->data[i * w + j]);
                j += 2;
                i--;
            }
            initialJRD--;
            if (initialJRD < 1) {
                initialJRD += 2;
                initialIRD--;
            }
        }
        compareResults = comparePGMImage(Un_1, Un);
        MPI_Allreduce(&compareResults, &flag, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
    }
    return Un_1;
}

PGMImage* cutU(PGMImage* U) {
    PGMImage* U0 = (PGMImage*)malloc(sizeof(PGMImage));
    U0->height = U->height - 2;
    U0->width = U->width - 2;
    U0->maxValue = U->maxValue;
    U0->data = (double*)malloc(U0->height * U0->width * sizeof(double));
    for (int i = 0; i < U0->height; i++) {
        for (int j = 0; j < U0->width; j++) {
            U0->data[i * U0->width + j] = U->data[(i+1) * U->width + j+1];
        }
    }
    return U0;
}

PGMImage* growU(PGMImage* U) {
    PGMImage* U0 = (PGMImage*)malloc(sizeof(PGMImage));
    U0->height = U->height + 2;
    U0->width = U->width + 2;
    U0->maxValue = U->maxValue;
    U0->data = (double*)malloc(U0->height * U0->width * sizeof(double));
    for (int i = 0; i < U->height; i++) {
        for (int j = 0; j < U->width; j++) {
            U0->data[(i + 1) * U0->width + j + 1] = U->data[i * U->width + j];
        }
    }
    for (int i = 0; i < U0->height; i++) {
        U0->data[i * U0->width + 0] = INFINITY;
        U0->data[i * U0->width + U0->width - 1] = INFINITY;
    }
    for (int j = 0; j < U0->width; j++) {
        U0->data[0 * U0->width + j] = INFINITY;
        U0->data[(U0->height - 1) * U0->width + j] = INFINITY;
    }
    return U0;
}