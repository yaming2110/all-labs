close all;
clear all;
array_str = ["Cell_585_380.pgm", "Cell_1170_761.pgm", "Cell_2340_1522.pgm", "Cell_4680_3044.pgm"];
for i=1:4
    str = " -np 8 lab3_MPI.exe 0 ";
    str = strcat(str, array_str(i));
    for j = 1:20
        l=j
        system(strcat("C:\""Program Files""\""Microsoft MPI""\Bin\mpiexec.exe",str));
    end
    str1 = "timesStamps_p08.txt";
    fileID = fopen(str1,'r');
    formatSpec = '%f';
    buf = fscanf(fileID, formatSpec);
    fclose(fileID);
    delete timesStamps_p08.txt;
    timep(1,i)=min(buf);
    timep(2,i)=mean(buf);
    timep(3,i)=max(buf);
end
speedupp=timep(2,1) ./timep;
efficiency = speedupp;
for i=1:4
    efficiency(:,i) = efficiency(:,i) / (i);
end
x=1:1:4;
figure;
plot(x, timep(1,:), 'LineWidth', 2);
hold on;
grid on;
plot(x, timep(2,:), 'LineWidth', 2);
plot(x, timep(3,:), 'LineWidth', 2);
xlabel("p");
title("Time");
legend("Minimal time", "Average time", "Maximal time");

figure;
plot(x, speedupp(1,:), 'LineWidth', 2);
hold on;
grid on;
plot(x, speedupp(2,:), 'LineWidth', 2);
plot(x, speedupp(3,:), 'LineWidth', 2);
xlabel("p");
title("Speedup");
legend("Minimal time", "Average time", "Maximal time");

figure;
plot(x, efficiency(1,:), 'LineWidth', 2);
hold on;
grid on;
plot(x, efficiency(2,:), 'LineWidth', 2);
plot(x, efficiency(3,:), 'LineWidth', 2);
xlabel("p");
title("Efficiency");
legend("Minimal time", "Average time", "Maximal time");