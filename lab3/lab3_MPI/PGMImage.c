#include "PGMImage.h"
#include <stdlib.h>
#include "math.h"
#include <stdbool.h>
#include <ctype.h>

PGMImage* createPGMImage(int height, int width) {
    PGMImage* pgm = (PGMImage*)malloc(sizeof(PGMImage));
    pgm->height = height;
    pgm->width = width;
    pgm->maxValue = 255;
    pgm->data = (double*)malloc(height * width * sizeof(double));
    return pgm;
}

void ignoreComments(FILE* fp)
{
    int ch;
    char line[100];

    while ((ch = fgetc(fp)) != EOF && isspace(ch));
    if (ch == '#') {
        fgets(line, sizeof(line), fp);
        ignoreComments(fp);
    }
    else
        fseek(fp, -1, SEEK_CUR);
}

PGMImage* openPGM(const char* filename)
{
    PGMImage* pgm = (PGMImage*)malloc(sizeof(PGMImage));
    FILE* pgmfile = fopen(filename, "rb");
    int buf;
    char trash[3];
    ignoreComments(pgmfile);
    fscanf(pgmfile, "%s", trash);
    ignoreComments(pgmfile);
    fscanf(pgmfile, "%d %d", &(pgm->width), &(pgm->height));
    ignoreComments(pgmfile);
    fscanf(pgmfile, "%d", &(pgm->maxValue));
    ignoreComments(pgmfile);
    pgm->data = (double*)malloc(pgm->height * pgm->width * sizeof(double));
    for (int i = 0; i < pgm->height; i++) {
        for (int j = 0; j < pgm->width; j++) {
            buf = fgetc(pgmfile);
            pgm->data[i * pgm->width + j] = (double)(pgm->maxValue - buf);
            /*pgm->data[i * pgm->width + j] = (double)buf;*/
        }
    }
    fclose(pgmfile);
    return pgm;
}


void writePGM(PGMImage* pgm, const char* filename)
{
    FILE* pgmFile;
    int i, j;

    pgmFile = fopen(filename, "wb");
    fprintf(pgmFile, "P5 ");
    fprintf(pgmFile, "%d %d ", pgm->width, pgm->height);
    fprintf(pgmFile, "%d ", pgm->maxValue);
    int buf;
    double max = -1;
    for (i = 0; i < pgm->height; i++) {
        for (j = 0; j < pgm->width; j++) {
            if (max < pgm->data[i * pgm->width + j])
                max = pgm->data[i * pgm->width + j];
        }

    }
    for (i = 0; i < pgm->height; i++) {
        for (j = 0; j < pgm->width; j++) {
            pgm->data[i * pgm->width + j] *= pgm->maxValue / max;
        }

    }
    for (i = 0; i < pgm->height; i++) {
        for (j = 0; j < pgm->width; j++) {
            buf = (int)round(pgm->data[i * pgm->width + j]);
            fputc((pgm->maxValue - buf) & 255, pgmFile);
            /*fputc(buf & 255, pgmFile);*/
        }
    }

    fclose(pgmFile);
}

void freePGM(PGMImage* pgm) {
    free(pgm->data);
    free(pgm);
}

PGMImage* scaleU2(PGMImage* pgm) {
    PGMImage* U = (PGMImage*)malloc(sizeof(PGMImage));
    U->height = pgm->height * 2;
    U->width = pgm->width * 2;
    U->maxValue = pgm->maxValue;
    U->data = (double*)malloc(U->height * U->width * sizeof(double));
    for (int i = 0; i < pgm->height; i++) {
        for (int j = 0; j < pgm->width; j++) {
            U->data[(2 * i) * U->width + (2 * j)] = pgm->data[i * pgm->width + j];
            U->data[(2 * i) * U->width + (2 * j + 1)] = pgm->data[i * pgm->width + j];
            U->data[(2 * i + 1) * U->width + (2 * j)] = pgm->data[i * pgm->width + j];
            U->data[(2 * i + 1) * U->width + (2 * j + 1)] = pgm->data[i * pgm->width + j];
            
        }
    }
    return U;
}

PGMImage* scaleD2(PGMImage* pgm) {
    PGMImage* U = (PGMImage*)malloc(sizeof(PGMImage));
    U->height = pgm->height / 2;
    U->width = pgm->width / 2;
    U->maxValue = pgm->maxValue;
    U->data = (double*)malloc(U->height * U->width * sizeof(double));
    for (int i = 0; i < U->height; i++) {
        for (int j = 0; j < U->width; j++) {
            int n = 1;
            U->data[i * U->width + j] = pgm->data[(2 * i) * pgm->width + (2 * j)];
            if (2 * j + 1 < pgm->width) {
                U->data[i * U->width + j] = pgm->data[(2 * i) * pgm->width + (2 * j + 1)];
                n++;
            }
            if (2 * i + 1 < pgm->height) {
                U->data[i * U->width + j] = pgm->data[(2 * i + 1) * pgm->width + (2 * j)];
                n++;
            }
            if (2 * j + 1 < pgm->width && 2 * i + 1 < pgm->height) {
                U->data[i * U->width + j] = pgm->data[(2 * i + 1) * pgm->width + (2 * j + 1)];
                n++;
            }
            U->data[i * U->width + j] /= n;

        }
    }
    return U;
}