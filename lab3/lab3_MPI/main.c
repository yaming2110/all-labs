#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "math.h"
#include "PGMImage.h"
#include "GWDT.h"
#include <windows.h>
#include <time.h>
#include "mpi.h"

int main(int argc, char const* argv[])
{
    MPI_Init(&argc, &argv);

    int numtask, rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &numtask);
    int width, height;
    int* numberStr = NULL;
    int* numberData = NULL;
    int* startData = NULL;
    PGMImage* pgm = NULL;
    PGMImage* U0 = NULL;
    const char* infile;
    const char* iofile;
    int flag = 0;

    infile = "Cell_1170_761.pgm";
    iofile = "Cell_1170_761Light.pgm";
    if (rank == 0) {
        if (argc > 1) {
            flag = atoi(argv[1]);
        }
        if (argc > 2) {
            infile = argv[2];
        }
        if (argc > 3) {
            iofile = argv[3];
        }
        pgm = openPGM(infile);
        U0 = createU0(pgm);
        width = pgm->width;
        int k = pgm->height / numtask;
        int q = pgm->height - numtask * k;
        numberStr = (int*)malloc(numtask * sizeof(int));
        numberData = (int*)malloc(numtask * sizeof(int));
        startData = (int*)malloc(numtask * sizeof(int));
        for (int i = 0; i < numtask; i++) {
            if (i < q) {
                startData[i] = (k + 1) * i * pgm->width;
                numberStr[i] = k + 1;
                numberData[i] = (k + 1) * pgm->width;
            }
            else {
                startData[i] = ((k + 1) * q + k * (i - q)) * pgm->width;
                numberStr[i] = k;
                numberData[i] = k * pgm->width;
            }
        }
    }
    MPI_Bcast(&width, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Scatter(numberStr, 1, MPI_INT, &height, 1, MPI_INT, 0, MPI_COMM_WORLD);
    PGMImage* cutPgm = createPGMImage(height, width);
    PGMImage* cutU0 = createPGMImage(height, width);
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0) {
        for (int i = 1; i < numtask; i++) {
            MPI_Send(pgm->data + startData[i], numberData[i], MPI_DOUBLE, i, i, MPI_COMM_WORLD);
            MPI_Send(U0->data + startData[i], numberData[i], MPI_DOUBLE, i, i, MPI_COMM_WORLD);
        }
        cutPgm->data = pgm->data;
        cutU0->data = U0->data;
    }
    else {
        MPI_Status s;
        MPI_Recv(cutPgm->data, height * width, MPI_DOUBLE, 0, rank, MPI_COMM_WORLD, &s);
        MPI_Recv(cutU0->data, height * width, MPI_DOUBLE, 0, rank, MPI_COMM_WORLD, &s);
    }
    PGMImage* cutU1 = growU(cutU0);
    if (rank == 0) {
        freePGM(U0);
    }
    else {
        freePGM(cutU0);
    }
    double timeStart = 0;
    double timeFinish = 0;
    timeStart = MPI_Wtime();
    PGMImage* U2 = GWDT(cutU1, cutPgm, rank, numtask);
    timeFinish = MPI_Wtime();
    double firstTimeStart = 0;
    double lastTimeFinish = 0;
    MPI_Reduce(&timeStart, &firstTimeStart, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
    MPI_Reduce(&timeFinish, &lastTimeFinish, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
    PGMImage* cutU2 = cutU(U2);
    free(U2);
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0) {
        MPI_Status s;
        for (int i = 1; i < numtask; i++) {
            MPI_Recv(pgm->data + startData[i], numberData[i], MPI_DOUBLE, i, i, MPI_COMM_WORLD, &s);
        }
        memcpy(pgm->data, cutU2->data, numberData[0] * sizeof(double));
    }
    else {
        MPI_Send(cutU2->data, height * width, MPI_DOUBLE, 0, rank, MPI_COMM_WORLD);
    }


    MPI_Barrier(MPI_COMM_WORLD);

    if (rank == 0) {
        if (flag) {
            writePGM(pgm, iofile);
        }
        free(numberData);
        free(numberStr);
        free(startData);
        freePGM(pgm);
        FILE* file = NULL;
        char fileName[] = "timesStamps_p01.txt";
        fileName[13] = '0' + numtask / 10;
        fileName[14] = '0' + numtask % 10;
        fopen_s(&file, fileName, "a");
        fprintf(file, "%lf\n", lastTimeFinish - firstTimeStart);
        fclose(file);
    }
    else {
        freePGM(cutPgm);
    }

    MPI_Finalize();
    return 0;
}