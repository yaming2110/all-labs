#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "math.h"

#include "Problem.h"

double** createInitialConditials(int N, int M) {
    double** A = (double**)malloc(N * sizeof(double*));
    for (int i = 0; i < N; i++) {
        A[i] = (double*)malloc(M * sizeof(double));
        for (int j = 0; j < M; j++) {
            A[i][j] = 0;
        }
    }
    double startPower = 10;
    for (int i = (int)round((double)(N - 1) * 3 / 16); i < (int)round((double)(N - 1) * 5 / 16); i++) {
        for (int j = (int)round((double)(M - 1) * 3 / 16); j < (int)round((double)(M - 1) * 5 / 16); j++) {
            A[i][j] = startPower;
        }
        for (int j = (int)round((double)(M - 1) * 11 / 16); j < (int)round((double)(M - 1) * 13 / 16); j++) {
            A[i][j] = startPower;
        }
    }

    for (int i = (int)round((double)(N - 1) * 11 / 16); i < (int)round((double)(N - 1) * 13 / 16); i++) {
        for (int j = (int)round((double)(M - 1) * 3 / 16); j < (int)round((double)(M - 1) * 5 / 16); j++) {
            A[i][j] = startPower;
        }
        for (int j = (int)round((double)(M - 1) * 11 / 16); j < (int)round((double)(M - 1) * 13 / 16); j++) {
            A[i][j] = startPower;
        }
    }
    return A;
}

problem* initializing(char** argv) {
    problem* P = (problem*)malloc(sizeof(problem));
    double xs = atof(argv[2]);
    double ys = atof(argv[3]);
    double xf = atof(argv[4]);
    double yf = atof(argv[5]);
    P->N = atoi(argv[6]);
    P->M = atoi(argv[7]);
    P->tEnd = atof(argv[8]);
    int T = atoi(argv[9]);
    double a = atof(argv[10]);
    P->ht = P->tEnd / T;
    double hx = (xf - xs) / (P->N - 1);
    double hy = (yf - ys) / (P->M - 1);
    P->un_1 = createInitialConditials(P->N, P->M);
    P->un = createInitialConditials(P->N, P->M);
    P->koefhx = a * a * P->ht / (hx * hx);
    P->koefhy = a * a * P->ht / (hy * hy);
    return P;
}
void nextIteration(problem* P) {
    double borderPower = 1;
    int i = 0;
#pragma omp parallel for private(i) collapse(2)
    for (i = 1; i < P->N - 1; i++) {
        for (int j = 1; j < P->M - 1; j++) {
                P->un[i][j] = P->koefhx * (P->un_1[i + 1][j] + P->un_1[i - 1][j]) + P->koefhy * (P->un_1[i][j - 1] + P->un_1[i][j + 1]) + P->un[i][j] * (1 - 2 * (P->koefhy + P->koefhx));
        }
    }
#pragma omp parallel for private(i)
    for (i = 0; i < P->N; i++) {
        P->un[i][0] = borderPower;
    }
#pragma omp parallel for private(i)
    for (i = 0; i < P->N; i++) {
        P->un[i][P->M - 1] = borderPower;
    }
#pragma omp parallel for private(i)
    for (i = 0; i < P->M; i++) {
        P->un[0][i] = borderPower;
    }
#pragma omp parallel for private(i)
    for (i = 0; i < P->M; i++) {
        P->un[P->N - 1][i] = borderPower;
    }

    double** buf;
    buf = P->un_1;
    P->un_1 = P->un;
    P->un = buf;
}

void freeProblem(problem* P) {
    for (int i = 0; i < P->N; i++) {
        free(P->un_1[i]);
    }
    free(P->un_1);

    for (int i = 0; i < P->N; i++) {
        free(P->un[i]);
    }
    free(P->un);
    free(P);
}