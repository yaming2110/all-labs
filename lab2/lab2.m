close all;
clear all;
xs = 0;
ys = 0;
xf = 1;
yf = 1;
N = 500;
M = 500;
tEnd = 80;
T =40000;
a=0.02;
flag = 1;
x=linspace(xs, xf,N);
y=linspace(ys, yf,M);
[X,Y]=meshgrid(x,y);
str = "lab2.exe "+ num2str(6) + " "+ num2str(xs) + " "+ num2str(ys) + " "+ num2str(xf) + " "+ num2str(yf) + " "+ num2str(N) + " "+ num2str(M) + " " + num2str(tEnd)+ " " + num2str(T)+ " " + num2str(a)+ " " + num2str(flag);
system(str)
for i=0:tEnd
    if i == 0 || i == tEnd/5 || i == tEnd /2 || i == tEnd
        str1 = "u_t" + num2str((i - rem(i,10)) / 10) + num2str(rem(i,10))+".txt";
        buf = readmatrix(str1);
        fig = figure;
        str2 = strcat("T = ", num2str(i));
        contourf(X,Y,buf, 'EdgeColor','none');
        title(str2);
        set(gca,'FontSize',36)
        set(findall(gcf,'type','text'),'FontSize',36)
        colorbar;
    end
end

delete timesStamps_p06.txt
tEnd = 10;
T = 5000;
flag = 0;
maxp = 24;

for i=1:maxp
    k=i
    nump = i;
    str = "lab2.exe "+ num2str(nump) + " "+ num2str(xs) + " "+ num2str(ys) + " "+ num2str(xf) + " "+ num2str(yf) + " "+ num2str(N) + " "+ num2str(M) + " " + num2str(tEnd)+ " " + num2str(T)+ " " + num2str(a)+ " " + num2str(flag);
    for j = 1:20
        l=j
        system(str);
    end
    str1 = "timesStamps_p" + num2str((nump - rem(nump,10)) / 10) + num2str(rem(nump,10))+".txt";
    fileID = fopen(str1,'r');
    formatSpec = '%f';
    buf = fscanf(fileID, formatSpec);
    fclose(fileID);
    timep(1,i)=min(buf);
    timep(2,i)=mean(buf);
    timep(3,i)=max(buf);
end
speedupp=timep(2,1) ./timep;
efficiency = speedupp;
for i=1:maxp
    efficiency(:,i) = efficiency(:,i) / (i);
end
x=1:1:maxp;
figure;
plot(x, timep(1,:));
hold on;
grid on;
plot(x, timep(2,:));
plot(x, timep(3,:));
xlabel("p");
title("Time");
legend("Minimal time", "Average time", "Maximal time");

figure;
plot(x, speedupp(1,:));
hold on;
grid on;
plot(x, speedupp(2,:));
plot(x, speedupp(3,:));
xlabel("p");
title("Speedup");
legend("Minimal time", "Average time", "Maximal time");

figure;
plot(x, efficiency(1,:));
hold on;
grid on;
plot(x, efficiency(2,:));
plot(x, efficiency(3,:));
xlabel("p");
title("Efficiency");
legend("Minimal time", "Average time", "Maximal time");