#pragma once
typedef struct {
	double** un_1;
	double** un;
	int N;
	int M;
	double koefhx;
	double koefhy;
	double tEnd;
	double ht;
}problem;

double** createInitialConditials(int N, int M);
problem* initializing(char** argv);
void nextIteration(problem* P);
void freeProblem(problem* P);