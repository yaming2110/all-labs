#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "math.h"
#include <time.h>

#include "Problem.h"
void printUn(double** un, int time, int N, int M) {
    char fileName[] = "u_t00.txt";
    fileName[3] = '0' + time / 10;
    fileName[4] = '0' + time % 10;
    FILE* file = NULL;
    fopen_s(&file, fileName, "w");
    for (int i = 0; i < N; i++) {
        fprintf(file, "%lf", un[i][0]);
        for (int j = 1; j < M; j++) {
            fprintf(file, ", %lf", un[i][j]);
        }
        fprintf(file, "\n");
    }
    fclose(file);
}

int main(int argc, char** argv) {
    if (argc == 12) {
        int number_of_threads = atoi(argv[1]);
        problem* P = initializing(argv);
        int flag = atoi(argv[11]);
        double t = 0;
        int iterator = 0;
        double allTime = 0;
        if (flag) {
            printUn(P->un_1, 0, P->N, P->M);
        }

        omp_set_num_threads(number_of_threads);

        while (t < P->tEnd) {
            clock_t begin = clock();
            nextIteration(P);
            clock_t end = clock();
            allTime += (double)(end - begin) / CLOCKS_PER_SEC;
            t += P->ht;
            if (t - (int)t < P->ht) {
                iterator++;
                if (flag) {
                    printUn(P->un_1, iterator, P->N, P->M);
                }
            }
        }
        FILE* file = NULL;
        char fileName[] = "timesStamps_p01.txt";
        fileName[13] = '0' + number_of_threads / 10;
        fileName[14] = '0' + number_of_threads % 10;
        fopen_s(&file, fileName, "a");
        fprintf(file, "%lf\n", allTime);
        fclose(file);


        freeProblem(P);
    }
    return 0;
}